% notice: program written in MATLAB

% Joanna Rocznik
% Technical Physics, Silesian University of Technology
% Gliwice, 2019

% please, credit the copied chunks of code
% but please, do n o t copy mindlessly
% this code is entirely my work, so do have some decency
% if you're planning on stealing it - karma will find you and do its magic

% created with the invaluable help of Julia Pluta and Beniamin Strączek

%_________________________________________________________________________

%1

clear
path = 'C:\Users\student\Desktop\jsonlab-1.9.8\jsonlab-1.9.8';
addpath(path);

empty = '              ';

city = {'CZESTOCHOWA', 'KATOWICE', 'RACIBORZ'};
source = {'Data from WTTR', 'Data from IMGW'};

head1 = {'STATION', 'CITY', 'DATE', 'HOUR', 'TEMPERATURE', 'FEELS-LIKE TEMPERATURE', 'PRESSURE', 'RELATIVE HUMIDITY', 'WIND SPEED'};
head2 = {' ', ' ', ' ', '    UTC', '    °C', '    °C', '    hPa', '    %', '    km/h'};

%_________________________________________________________________________

%1
% i'm introducing variables which i'm gonna be using later on in the code
% 'empty' is a string variable of 'spaces' used in order to ensure an 
%   empty cell in the csv file
% 'city', 'source', 'head1' and 'head2' are cell arrays consisting of
%   the titles of columns in the csv file; used later on in the code
%_________________________________________________________________________

%2

if exist('weatherData.csv') == 2
    break
else
    fid = fopen('weatherData.csv','w+');
    fprintf(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n', ...
    '"', head1{1}, '";"', head1{2}, '";"', head1{3}, '";"', head1{4}, '";"', head1{5}, '";"', head1{6}, '";"', head1{7}, '";"', head1{8}, '";"', head1{9}, '";"', empty, '";"', head1{1}, '";"', head1{2}, '";"', head1{3}, '";"', head1{4}, '";"', head1{5}, '";"', head1{6}, '";"', head1{7}, '";"', head1{8}, '";"', head1{9}, '";"', empty, '";"', head1{1}, '";"', head1{2}, '";"', head1{3}, '";"', head1{4}, '";"', head1{5}, '";"', head1{6}, '";"', head1{7}, '";"', head1{8}, '";"', head1{9}, '"');
    fprintf(fid,'%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n\n', ...
        '"', head2{1}, '";"', head2{2}, '";"', head2{3}, '";"', head2{4}, '";"', head2{5}, '";"', head2{6}, '";"', head2{7}, '";"', head2{8}, '";"', head2{9}, '";"', empty, '";"', head2{1}, '";"', head2{2}, '";"', head2{3}, '";"', head2{4}, '";"', head2{5}, '";"', head2{6}, '";"', head2{7}, '";"', head2{8}, '";"', head2{9}, '";"', empty, '";"', head2{1}, '";"', head2{2}, '";"', head2{3}, '";"', head2{4}, '";"', head2{5}, '";"', head2{6}, '";"', head2{7}, '";"', head2{8}, '";"', head2{9}, '"');
    fclose(fid);
end

%_________________________________________________________________________

%2
% i'm creating a csv file into which i'm gonna be exporting downloaded 
%   data as well as the cells created above
% the loop ensures that a file under the same name doesn't replace the
%   the already existing one and the data is added rather than rreplaced; 
%	the '== 2' is an indicator of a type of the file, as recognised by 
%	MATLAB
%_________________________________________________________________________

%3
 
wttr1 = urlread('http://wttr.in/Czestochowa?format=j1');
currentWeather1 = loadjson(wttr1);
cellConv1 = struct2cell(currentWeather1);
matConv1 = cell2mat(cellConv1{1});
    
t1 = matConv1.temp_C;
P1 = matConv1.pressure;
w1 = matConv1.windspeedKmph;
h1 = matConv1.humidity;

hour1 = matConv1.observation_time;
hourDec1 = sscanf(hour1, '%d');
if (hour1(7) == 'A' || (hourDec1 == 12) || (hourDec1 == 0));
    hourDec1 = hourDec1;
else
    hourDec1 = hourDec1 + 12;
end
T1 = num2str(hourDec1);

d1Cell{1, 2} =currentWeather1.weather{1, 1}.date;
d1 = cell2mat(d1Cell(1, 2));
    
feelsLikeCalc1 = (33 + (0.478 + (0.237 * sqrt(str2double(w1))) - (0.0124 * str2double(w1))) * ((str2double(t1) - 33)));
feelsLikeT1 = num2str(feelsLikeCalc1);

%-------------------------------------------------------------------------

wttr2 = urlread('http://wttr.in/Katowice?format=j1');
currentWeather2 = loadjson(wttr2);
cellConv2 = struct2cell(currentWeather2);
matConv2 = cell2mat(cellConv2{1});
    
t2 = matConv2.temp_C;
P2 = matConv2.pressure;
w2 = matConv2.windspeedKmph;
h2 = matConv2.humidity;

hour2 = matConv2.observation_time;
hourDec2 = sscanf(hour2, '%d');
if (hour2(7) == 'A' || (hourDec2 == 12) || (hourDec2 == 0));
    hourDec2 = hourDec2;
else
    hourDec2 = hourDec2 + 12;
end
T2 = num2str(hourDec2);

d2Cell{1, 2} = currentWeather2.weather{1, 1}.date;
d2 = cell2mat(d2Cell(1, 2));
    
feelsLikeCalc2 = (33 + (0.478 + (0.237 * sqrt(str2double(w2))) - (0.0124 * str2double(w2))) * ((str2double(t2) - 33)));
feelsLikeT2 = num2str(feelsLikeCalc2);

%-------------------------------------------------------------------------

wttr3 = urlread('http://wttr.in/Raciborz?format=j1');
currentWeather3 = loadjson(wttr3);
cellConv3 = struct2cell(currentWeather3);
matConv3 = cell2mat(cellConv3{1});
    
t3 = matConv3.temp_C;
P3 = matConv3.pressure;
w3 = matConv3.windspeedKmph;
h3 = matConv3.humidity;

hour3 = matConv3.observation_time;
hourDec3 = sscanf(hour3, '%d');
if (hour3(7) == 'A' || (hourDec3 == 12) || (hourDec3 == 0));
    hourDec3 = hourDec3;
else
    hourDec3 = hourDec3 + 12;
end
T3 = num2str(hourDec3);

d3Cell{1, 2} = currentWeather3.weather{1, 1}.date;
d3 = cell2mat(d3Cell(1, 2));
    
feelsLikeCalc3 = (33 + (0.478 + (0.237 * sqrt(str2double(w3))) - (0.0124 * str2double(w3))) * ((str2double(t3) - 33)));
feelsLikeT3 = num2str(feelsLikeCalc3);

%-------------------------------------------------------------------------
%-------------------------------------------------------------------------

imgw1 = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12550/format/json');
currentWeather4 = loadjson(imgw1);
	
t4 = currentWeather4.temperatura;
P4 = currentWeather4.cisnienie;
w4 = currentWeather4.predkosc_wiatru;
h4 = currentWeather4.wilgotnosc_wzgledna;
T4 = currentWeather4.godzina_pomiaru;
d4 = currentWeather4.data_pomiaru;

feelsLikeCalc4 = (33 + (0.478 + (0.237 * sqrt(str2double(w4))) - (0.0124 * str2double(w4))) * ((str2double(t4) - 33)));
feelsLikeT4 = num2str(feelsLikeCalc4);

%_________________________________________________________________________

imgw2 = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12560/format/json');
currentWeather5 = loadjson(imgw2);
	
t5 = currentWeather5.temperatura;
P5 = currentWeather5.cisnienie;
w5 = currentWeather5.predkosc_wiatru;
h5 = currentWeather5.wilgotnosc_wzgledna;
T5 = currentWeather5.godzina_pomiaru;
d5 = currentWeather5.data_pomiaru;

feelsLikeCalc5 = (33 + (0.478 + (0.237 * sqrt(str2double(w5))) - (0.0124 * str2double(w5))) * ((str2double(t5) - 33)));
feelsLikeT5 = num2str(feelsLikeCalc5);
    
%-------------------------------------------------------------------------

imgw3 = urlread('https://danepubliczne.imgw.pl/api/data/synop/id/12540/format/json');
currentWeather6 = loadjson(imgw3);
	
t6 = currentWeather6.temperatura;
P6 = currentWeather6.cisnienie;
w6 = currentWeather6.predkosc_wiatru;
h6 = currentWeather6.wilgotnosc_wzgledna;
T6 = currentWeather6.godzina_pomiaru;
d6 = currentWeather6.data_pomiaru;

feelsLikeCalc6 = (33 + (0.478 + (0.237 * sqrt(str2double(w6))) - (0.0124 * str2double(w6))) * ((str2double(t6) - 33)));
feelsLikeT6 = num2str(feelsLikeCalc6);

%_________________________________________________________________________

%3
% for both sites (six different URLs) i'm downloading and converting
%    data regarding current conditions - temperature, pressure, wind 
%    speed, relative humidity, time and date of measurement
% later, based on the downloaded data, i'm calculating the feels-like
%   temperature; then i'm converting that value into string in order for
%   it to match the rest of the data
% for wttr.in i'm changing the time format from 12-hour to 24-hour in 
%   a loop; if the hour of a given measurement is a before-noon one, the
%   'AM' bit is removed as well as the minute part - so that only the hour
%   remains - unchanged; for the past-noon hours i'm doing the same thing
%   as before, but i'm also adding 12 to the hour in order to get the 
%   24-hour format i was going for

%_________________________________________________________________________

%4

dataMatrixWTTR = ['"', source{1}, '";"', city{1}, '";"', d1, '";"', T1, '";"', t1, '";"', feelsLikeT1, '";"', P1, '";"', h1, '";"', w1, '";"', ...
    empty, '";"', source{1}, '";"', city{2}, '";"', d2, '";"', T2, '";"', t2, '";"', feelsLikeT2, '";"', P2, '";"', h2, '";"', w2, '";"', ...
    empty, '";"', source{1}, '";"', city{3}, '";"', d3, '";"', T3, '";"', t3, '";"', feelsLikeT3, '";"', P3, '";"', h3, '";"', w3, '"'];

dataMatrixIMGW = ['"', source{2}, '";"', city{1}, '";"', d4, '";"', T4, '";"', t4, '";"', feelsLikeT4, '";"', P4, '";"', h4, '";"', w4, '";"', ...
    empty, '";"', source{2}, '";"', city{2}, '";"', d5, '";"', T5, '";"', t5, '";"', feelsLikeT5, '";"', P5, '";"', h5, '";"', w5, '";"', ...
    empty, '";"', source{2}, '";"', city{3}, '";"', d6, '";"', T6, '";"', t6, '";"', feelsLikeT6, '";"', P6, '";"', h6, '";"', w6, '"'];

%_________________________________________________________________________

%4
% i'm creating two matrices - one containing data from wttr.in and the
%   other from imgw.pl

%_________________________________________________________________________

%5

for x = 1:1E12 %roboczo 3
    dlmwrite('weatherData.csv', dataMatrixWTTR, '-append', 'delimiter', '');
    dlmwrite('weatherData.csv', dataMatrixIMGW, '-append', 'delimiter', '');
    
    pause(3600); %roboczo 2
end

%_________________________________________________________________________

%5
% i'm writing both matrices into the csv file; in it data is grouped by
%   city
% the loop repeats itself loads of times, once every hour; the 'pause'
%   function pauses the loop for 3600 seconds

%_________________________________________________________________________

%6

filePath = 'C:\Users\student\Documents\MATLAB\weatherData.csv';
fileId = fopen(filePath);
C = textscan(fileId, '%q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q %q', 'delimiter', ';');

S = size(C{1})-2;
rows = S(1, 1);

%_________________________________________________________________________

%6
% now, i'm opening and reading the very same file i'd just created/
%   reopened
% the size 'S' of the first column (C{1}) in the file is a matrix; the
%	subtracted '2' indicates the first two rows that ALWAYS consist of 
%	words - impossible to convert to numerical values
% for further use, however, i only need the number of rows in it - thus
%	the variable'rows'

%_________________________________________________________________________

%7

for n = 1:rows;
    DataC(n, 1) = datenum(cell2mat(C{3}(n+2)));
    DataC(n, 2) = str2num(cell2mat(C{4}(n+2)));
    DataC(n, 3) = str2num(cell2mat(C{5}(n+2)));
    DataC(n, 4) = str2num(cell2mat(C{6}(n+2)));
    DataC(n, 5) = str2num(cell2mat(C{7}(n+2)));
    DataC(n, 6) = str2num(cell2mat(C{8}(n+2)));
    
    DataK(n, 1) = datenum(cell2mat(C{13}(n+2)));
    DataK(n, 2) = str2num(cell2mat(C{14}(n+2)));
    DataK(n, 3) = str2num(cell2mat(C{15}(n+2)));
    DataK(n, 4) = str2num(cell2mat(C{16}(n+2)));
    DataK(n, 5) = str2num(cell2mat(C{17}(n+2)));
    DataK(n, 6) = str2num(cell2mat(C{18}(n+2)));
    
    DataR(n, 1) = datenum(cell2mat(C{23}(n+2)));
    DataR(n, 2) = str2num(cell2mat(C{24}(n+2)));
    DataR(n, 3) = str2num(cell2mat(C{25}(n+2)));
    DataR(n, 4) = str2num(cell2mat(C{26}(n+2)));
    DataR(n, 5) = str2num(cell2mat(C{27}(n+2)));
    DataR(n, 6) = str2num(cell2mat(C{28}(n+2)));
end

%_________________________________________________________________________

%7
% in the loop above i'm creating three matrices containing data for each
%   city; with C standing for 'Czestochowa', K for 'Katowice' and R - for 
%   'Raciborz'
% each matrix consists of numerical values: date in MATLAB format, an hour,
%   temperature, feels-like temperature, pressure and relative humidity
% matrices are created for the purpose of extraction of the biggest 
%	values of the above

%_________________________________________________________________________

%8

maxTemperatureC = max(DataC(:, 3));
for cC1 = 1:rows;
    if DataC(cC1, 3) == maxTemperatureC;
        rC1 = cC1;
    end
end

maxFeelsLikeTC = max(DataC(:, 4));
for cC2 = 1:rows;
    if DataC(cC2, 4) == maxFeelsLikeTC;
        rC2 = cC2;
    end
end

maxPressureC = max(DataC(:, 5));
for cC3 = 1:rows;
    if DataC(cC3, 5) == maxPressureC;
        rC3 = cC3;
    end
end

maxHumidityC = max(DataC(:, 6));
for cC4 = 1:rows;
    if DataC(cC4, 6) == maxHumidityC;
        rC4 = cC4;
    end
end

%-------------------------------------------------------------------------

maxTemperatureK = max(DataK(:, 3));
for cK1 = 1:rows;
    if DataK(cK1, 3) == maxTemperatureK;
        rK1 = cK1;
    end
end

maxFeelsLikeTK = max(DataK(:, 4));
for cK2 = 1:rows;
    if DataK(cK2, 4) == maxFeelsLikeTK;
        rK2 = cK2;
    end
end

maxPressureK = max(DataK(:, 5));
for cK3 = 1:rows;
    if DataK(cK3, 5) == maxPressureK;
        rK3 = cK3;
    end
end

maxHumidityK = max(DataK(:, 6));
for cK4 = 1:rows;
    if DataK(cK4, 6) == maxHumidityK;
        rK4 = cK4;
    end
end

%-------------------------------------------------------------------------

maxTemperatureR = max(DataR(:, 3));
for cR1 = 1:rows;
    if DataR(cR1, 3) == maxTemperatureR;
        rR1 = cR1;
    end
end

maxFeelsLikeTR = max(DataR(:, 4));
for cR2 = 1:rows;
    if DataR(cR2, 4) == maxFeelsLikeTR;
        rR2 = cR2;
    end
end

maxPressureR = max(DataR(:, 5));
for cR3 = 1:rows;
    if DataR(cR3, 5) == maxPressureR;
        rR3 = cR3;
    end
end

maxHumidityR = max(DataR(:, 6));
for cR4 = 1:rows;
    if DataR(cR4, 6) == maxHumidityR;
        rR4 = cR4;
    end
end

%_________________________________________________________________________

%8
% here i'm looking for the maximum values of temperature, feels-like
%   temperature, pressure and relative humidity for each city separately
% in the loops i'm looking for the corresponding to each of the maximums
%   date
% all of the above actions involve the 'Data[city]' matrices only

%_________________________________________________________________________

%9

if maxTemperatureC > maxTemperatureK && maxTemperatureC > maxTemperatureR
    fprintf('The maximum temperature was %.2f °C and it occurred on %s at %.f in Czestochowa.\n', maxTemperatureC, datestr(DataC(rC1, 1)), DataC(rC1, 2))
elseif maxTemperatureK > maxTemperatureC && maxTemperatureK > maxTemperatureR
    fprintf('The maximum temperature was %.2f °C and it occurred on %s at %.f in Katowice.\n', maxTemperatureK, datestr(DataK(rK1, 1)), DataK(rK1, 2))
else
    fprintf('The maximum temperature was %.2f °C and it occurred on %s at %.f in Raciborz.\n', maxTemperatureR, datestr(DataR(rR1, 1)), DataR(rR1, 2))
end

if maxFeelsLikeTC > maxFeelsLikeTK && maxFeelsLikeTC > maxFeelsLikeTR
    fprintf('The maximum feels-like temperature was %.2f °C and it occurred on %s at %.f in Czestochowa.\n', maxFeelsLikeTC, datestr(DataC(rC2, 1)), DataC(rC2, 2))
elseif maxFeelsLikeTK > maxFeelsLikeTC && maxFeelsLikeTK > maxFeelsLikeTR
    fprintf('The maximum feels-like temperature was %.2f °C and it occurred on %s at %.f in Katowice.\n', maxFeelsLikeTK, datestr(DataK(rK2, 1)), DataK(rK2, 2))
else
    fprintf('The maximum feels-like temperature was %.2f °C and it occurred on %s at %.f in Raciborz.\n', maxFeelsLikeTR, datestr(DataR(rR2, 1)), DataR(rR2, 2))
end

if maxPressureC > maxPressureK && maxPressureC > maxPressureR
    fprintf('The maximum pressure was %.2f hPa and it occurred on %s at %.f in Czestochowa.\n', maxPressureC, datestr(DataC(rC3, 1)), DataC(rC3, 2))
elseif maxPressureK > maxPressureC && maxPressureK > maxPressureR
    fprintf('The maximum pressure was %.2f hPa and it occurred on %s at %.f in Katowice.\n', maxPressureK, datestr(DataK(rK3, 1)), DataK(rK3, 2))
else
    fprintf('The maximum pressure was %.2f hPa and it occurred on %s at %.f in Raciborz.\n', maxPressureR, datestr(DataR(rR3, 1)), DataR(rR3, 2))
end

if maxHumidityC > maxHumidityK && maxHumidityC > maxHumidityR
    fprintf('The maximum relative humidity was %.2f percent and it occurred on %s at %.f in Czestochowa.\n', maxHumidityC, datestr(DataC(rC4, 1)), DataC(rC4, 2))
elseif maxHumidityK > maxHumidityC && maxHumidityK > maxHumidityR
    fprintf('The maximum relative humidity was %.2f percent and it occurred on %s at %.f in Katowice.\n', maxHumidityK, datestr(DataK(rK4, 1)), DataK(rK4, 2))
else
    fprintf('The maximum realtive humidity was %.2f percent and it occurred on %s at %.f in Raciborz.\n', maxHumidityR, datestr(DataR(rR4, 1)), DataR(rR4, 2))
end

%_________________________________________________________________________

%9
% here i'm looking for the biggest value of each of the maximums and 
%   assigning a city to where it occurred
% using the 'fprintf' function i'm printing on the screen the information
%   regarding the biggest values of temperature, feels-like temperature,
%   pressure and relative humidity